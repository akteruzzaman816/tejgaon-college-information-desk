package me.assaduzzaman.tejgaoncollegeinformationdesk.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.CallActivity;
import me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass.FacultyMember;
import me.assaduzzaman.tejgaoncollegeinformationdesk.R;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    Context context;
    ArrayList<FacultyMember> member;


    public CustomAdapter(Context context, ArrayList<FacultyMember> member) {
        this.context = context;
        this.member = member;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.row_list, viewGroup, false);


        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {


        myViewHolder.name.setText(member.get(i).getFacultyMemberName());
        myViewHolder.designation.setText(member.get(i).getFacultyMemberDesignation());
        myViewHolder.mobile.setText(member.get(i).getFacultyMemberPhone());

        myViewHolder.callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String number=myViewHolder.mobile.getText().toString();
                Intent intent=new Intent(context,CallActivity.class);
                intent.putExtra("number",number);

                context.startActivity(intent);


            }
        });


    }

    @Override
    public int getItemCount() {
        return member.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView name,designation,mobile;
        FloatingActionButton callButton;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name=itemView.findViewById(R.id.teacher_name);
            designation=itemView.findViewById(R.id.teacher_designation);
            mobile=itemView.findViewById(R.id.teacher_number);
            callButton=itemView.findViewById(R.id.callButton);

        }


    }
}
