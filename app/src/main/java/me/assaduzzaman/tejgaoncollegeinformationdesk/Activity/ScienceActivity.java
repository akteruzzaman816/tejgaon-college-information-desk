package me.assaduzzaman.tejgaoncollegeinformationdesk.Activity;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import me.assaduzzaman.tejgaoncollegeinformationdesk.R;

public class ScienceActivity extends AppCompatActivity {
    CustomDrawerButton customDrawerButton;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_science);
        drawer= (DrawerLayout)findViewById(R.id.drawer_layout);
        customDrawerButton = (CustomDrawerButton)findViewById(R.id.menuimage);
        customDrawerButton.setDrawerLayout( drawer );
        customDrawerButton.getDrawerLayout().addDrawerListener( customDrawerButton );
        customDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDrawerButton.changeState();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void authority(View view) {
        startActivity(new Intent(ScienceActivity.this,AuthorityActivity.class));
    }

    public void faculty(View view) {
        startActivity(new Intent(ScienceActivity.this,FacultyActivity.class));
    }

    public void staff(View view) {
        startActivity(new Intent(ScienceActivity.this,StaffActivity.class));
    }

    public void about(View view) {
        startActivity(new Intent(ScienceActivity.this,AboutActivity.class));
    }

    public void contact(View view) {
        startActivity(new Intent(ScienceActivity.this,ContactActivity.class));
    }

    public void botany(View view) {

        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",4);
        startActivity(intent);
    }
    public void philosophy(View view) {

        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",22);
        startActivity(intent);
    }

    public void bio(View view) {

        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",5);
        startActivity(intent);
    }

    public void chemistry(View view) {

        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",6);
        startActivity(intent);
    }

    public void eng(View view) {

//        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
//        intent.putExtra("id",404);
//        startActivity(intent);
    }

    public void geo(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",11);
        startActivity(intent);
    }

    public void home(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",13);
        startActivity(intent);
    }

    public void math(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",18);
        startActivity(intent);
    }

    public void phy(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",19);
        startActivity(intent);
    }

    public void psy(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",20);
        startActivity(intent);
    }

    public void stat(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",23);
        startActivity(intent);
    }

    public void zoology(View view) {
        Intent intent=new Intent(ScienceActivity.this,ListActivity.class);
        intent.putExtra("id",28);
        startActivity(intent);
    }
}
