package me.assaduzzaman.tejgaoncollegeinformationdesk.Activity;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;

import me.assaduzzaman.tejgaoncollegeinformationdesk.Adapter.CustomStaffAdapter;
import me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass.OfficeStaff;
import me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass.OfficeStaffs;
import me.assaduzzaman.tejgaoncollegeinformationdesk.R;

public class StaffListActivity extends AppCompatActivity {

    CustomDrawerButton customDrawerButton;
    DrawerLayout drawer;
    OfficeStaffs members;
    CustomStaffAdapter adapter;
    OfficeStaff officeStaff;
    RecyclerView recyclerView;
    ArrayList<OfficeStaff> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_list);


        int id= (int) getIntent().getIntExtra("id",0);




        recyclerView=findViewById(R.id.recylerView);
        recyclerView.setNestedScrollingEnabled(false);

        members=new OfficeStaffs();
        list=members.getOfficeStaffsById(id);



        adapter=new CustomStaffAdapter(StaffListActivity.this,list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setAdapter(adapter);


        drawer= (DrawerLayout)findViewById(R.id.drawer_layout);
        customDrawerButton = (CustomDrawerButton)findViewById(R.id.menuimage);
        customDrawerButton.setDrawerLayout( drawer );
        customDrawerButton.getDrawerLayout().addDrawerListener( customDrawerButton );
        customDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDrawerButton.changeState();
            }
        });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void authority(View view) {
        startActivity(new Intent(StaffListActivity.this,AuthorityActivity.class));
    }

    public void faculty(View view) {
        startActivity(new Intent(StaffListActivity.this,FacultyActivity.class));
    }

    public void staff(View view) {
        startActivity(new Intent(StaffListActivity.this,StaffActivity.class));
    }

    public void about(View view) {
        startActivity(new Intent(StaffListActivity.this,AboutActivity.class));
    }

    public void contact(View view) {
        startActivity(new Intent(StaffListActivity.this,ContactActivity.class));
    }
}
