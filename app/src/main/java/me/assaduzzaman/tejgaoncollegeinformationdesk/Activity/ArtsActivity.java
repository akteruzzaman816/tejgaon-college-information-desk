package me.assaduzzaman.tejgaoncollegeinformationdesk.Activity;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import me.assaduzzaman.tejgaoncollegeinformationdesk.R;

public class ArtsActivity extends AppCompatActivity {
    CustomDrawerButton customDrawerButton;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arts);
        drawer= (DrawerLayout)findViewById(R.id.drawer_layout);
        customDrawerButton = (CustomDrawerButton)findViewById(R.id.menuimage);
        customDrawerButton.setDrawerLayout( drawer );
        customDrawerButton.getDrawerLayout().addDrawerListener( customDrawerButton );
        customDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDrawerButton.changeState();
            }
        });
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    public void authority(View view) {
        startActivity(new Intent(ArtsActivity.this,AuthorityActivity.class));
    }

    public void faculty(View view) {
        startActivity(new Intent(ArtsActivity.this,FacultyActivity.class));
    }

    public void staff(View view) {
        startActivity(new Intent(ArtsActivity.this,StaffActivity.class));
    }

    public void about(View view) {
        startActivity(new Intent(ArtsActivity.this,AboutActivity.class));
    }

    public void contact(View view) {
        startActivity(new Intent(ArtsActivity.this,ContactActivity.class));
    }



    public void bangla(View view) {

        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",2);
        startActivity(intent);
    }


    public void English(View view) {
        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",8);
        startActivity(intent);
    }

    public void sociology(View view) {
        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",24);
        startActivity(intent);
    }

    public void IslamiStudies(View view) {
        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",15);
        startActivity(intent);
    }

    public void IslamicHistory(View view) {

        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",14);
        startActivity(intent);
    }

    public void HomeEconomics(View view) {
        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",13);
        startActivity(intent);
    }

    public void History(View view) {
        Intent intent=new Intent(ArtsActivity.this,ListActivity.class);
        intent.putExtra("id",12);
        startActivity(intent);
    }
}
