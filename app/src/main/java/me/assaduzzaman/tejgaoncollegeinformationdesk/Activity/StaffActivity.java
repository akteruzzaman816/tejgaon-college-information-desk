package me.assaduzzaman.tejgaoncollegeinformationdesk.Activity;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import me.assaduzzaman.tejgaoncollegeinformationdesk.R;

public class StaffActivity extends AppCompatActivity {

    CustomDrawerButton customDrawerButton;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff);



        drawer= (DrawerLayout)findViewById(R.id.drawer_layout);
        customDrawerButton = (CustomDrawerButton)findViewById(R.id.menuimage);
        customDrawerButton.setDrawerLayout( drawer );
        customDrawerButton.getDrawerLayout().addDrawerListener( customDrawerButton );
        customDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDrawerButton.changeState();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void authority(View view) {
        startActivity(new Intent(StaffActivity.this,AuthorityActivity.class));
    }

    public void faculty(View view) {
        startActivity(new Intent(StaffActivity.this,FacultyActivity.class));
    }

    public void staff(View view) {
        startActivity(new Intent(StaffActivity.this,StaffActivity.class));
    }

    public void about(View view) {
        startActivity(new Intent(StaffActivity.this,AboutActivity.class));
    }

    public void contact(View view) {
        startActivity(new Intent(StaffActivity.this,ContactActivity.class));
    }





    public void class4staffs(View view) {
        Intent intent=new Intent(StaffActivity.this,StaffListActivity.class);
        intent.putExtra("id",4);
        startActivity(intent);
    }

    public void class3staffs(View view) {

        Intent intent=new Intent(StaffActivity.this,StaffListActivity.class);
        intent.putExtra("id",3);
        startActivity(intent);

    }

    public void officers(View view) {

        Intent intent=new Intent(StaffActivity.this,StaffListActivity.class);
        intent.putExtra("id",2);
        startActivity(intent);
    }
}
