package me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass;

public class OfficeStaff {
    private int staffID;
    private String staffName;
    private String staffDesignation;
    private String staffPhoneNumber;

    public OfficeStaff(int staffID, String staffName, String staffDesignation, String staffPhoneNumber) {
        this.staffID = staffID;
        this.staffName = staffName;
        this.staffDesignation = staffDesignation;
        this.staffPhoneNumber = staffPhoneNumber;
    }

    public int getStaffID() {
        return staffID;
    }

    public String getStaffName() {
        return staffName;
    }

    public String getStaffDesignation() {
        return staffDesignation;
    }

    public String getStaffPhoneNumber() {
        return staffPhoneNumber;
    }
}
