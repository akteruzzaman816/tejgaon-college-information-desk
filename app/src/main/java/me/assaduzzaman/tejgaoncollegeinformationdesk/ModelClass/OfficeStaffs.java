package me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass;

import java.util.ArrayList;

public class OfficeStaffs {

    ArrayList<OfficeStaff> allOfficeStaffs;

    public ArrayList<OfficeStaff> getOfficeStaffsById(int id) {

        ArrayList<OfficeStaff> officeStaffs=new ArrayList<>();

        for (int i=0; i< allOfficeStaffs.size(); i++)
        {
            if(allOfficeStaffs.get(i).getStaffID() == id)
                officeStaffs.add(allOfficeStaffs.get(i));

        }

        return officeStaffs;
    }

    public OfficeStaffs() {

        allOfficeStaffs=new ArrayList<>();


        // Data for the officers.........................

        allOfficeStaffs.add(new OfficeStaff(2, "Md. Ashraf Ali","Administrator Officer" ,"+8801798144072"));
        allOfficeStaffs.add(new OfficeStaff(2, "Md. Jakir Hossen","Account Officer" ,"+8801818428055"));
        allOfficeStaffs.add(new OfficeStaff(2, "Md. Billal Hossen","Imam" ,"+8801817750548"));




        //  Data for the Class 3 Staffs.............................
        allOfficeStaffs.add(new OfficeStaff(3, "Nazrul Islam","Library Assistant" ,"+8801718436465"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md. Shahinur Kabir","Library Assistant" ,"+8801716829212"));
        allOfficeStaffs.add(new OfficeStaff(3, "Afsana Khan","Library Assistant" ,"+8801718018823"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md. Fazlur Rahman","Account Assistant" ,"+8801552419226"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Tajul Islam","Office Assistant" ,"+8801819126453"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Sayef Ullah","Clerk" ,"+8801917292837"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Shahadat Hossain","Office Assistant" ,"+8801819823480"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Kamrul Hasan","Office Assistant" ,"+8801716443887"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mohibul Islam Chow.","Office Assistant" ,"+8801676991518"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Fazlay Rabbi","Office Assistant" ,"+8801199019045"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Maynul Islam","Office Assistant" ,"+8801718489356"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mohibul Hossain","Office Assistant" ,"+8801711446509"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Abdul Malek","Office Assistant" ,"+8801675764461"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Saiful Islam","Office Assistant" ,"+8801720542884"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Robiul Islam","Office Assistant" ,"+8801715331488"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Firoz ","Office Assistant" ,"+8801716371408"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Raich Uddin-1","Store & Transport Incharge" ,"+8801720549215"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mizanur Rahman Akanda","Clerk cum Typist " ,"+8801711789452"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Abdul Aziz","Office Assistant" ,"+8801715926119"));
        allOfficeStaffs.add(new OfficeStaff(3, "Parimal Kumar Bassar"," Office Assistant" ,"+8801552412435"));
        allOfficeStaffs.add(new OfficeStaff(3, "Nurrunnahar"," Library Assistant" ,"+8801817523286"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Moniruzzaman","Computer Operator" ,"+8801720518910"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Bazlur Rashid"," Office Assistant" ,"+8801714342432"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Ibrahim"," Office Assistant" ,"+8801716500804"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mozibor Rahman"," Office Assistant" ,"+8801839300036"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Nurer Rahman","Office Assistant" ,"+8801815362627"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Azizul Haque"," Office Assistant" ,"+8801681909023"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Raich Uddin-2"," Office Assistant" ,"+8801729479637"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Khaled Azmal","Office Assistant" ,"+8801925992648"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mozzammel","Office Assistant" ,"+8801712032364"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Anwar Hossain","Section Assistant" ,"+8801711044319"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Hafizur Rahman","Clerk cum typist" ,"+8801922477504"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Ilias","Imam" ,"+8801721493138"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Monjurul Islam","Office Assistant" ,"+8801716879369"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Sojon mia"," Office Assistant" ,"+8801737872167"));
        allOfficeStaffs.add(new OfficeStaff(3, "Sohan mia"," Office Assistant" ,"+8801925731423"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Nazrul Islam"," Office Assistant" ,"+8801740158122"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Johurul Islam","PA-EU" ,"+8801989422340"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Morshed Alam","Office Assistant" ,"+8801774252962"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Shahjalal Uddin Ahmed","Office Assistant" ,"+8801672993831"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mokhlasur Rahaman","Office Assistant" ,"+8801718967752"));

        allOfficeStaffs.add(new OfficeStaff(3, "Md.Foysal Arafat","Co-ordinate Assistant" ,"+8801681007831"));

        allOfficeStaffs.add(new OfficeStaff(3, "Mirza Shahina Parvin","Office Assistant" ,"+8801712228234"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Yeasin Arafat","Office Assistant" ,"+8801684039179"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Abul Basar","Office Assistant" ,"+8801914262132"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Faruk Hossain","Office Assistant" ,"+8801733170381"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Mokhlasur Rahaman - 2","Office Assistant" ,"+8801735525013"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Jaharul Islam","Office Assistant" ,"+8801739978530"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Zakir Hossain","Office Assistant" ,"+8801981702442"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Iqbal Hossain","Driver" ,"+8801911789313"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Masud Reza","Driver" ,"+8801918191837"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Yonnus Ali","Driver" ,"+8801937924113"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Firoz Mahamud","Driver" ,"+8801728021206"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Anamul Haque","Driver" ,"+8801935735251"));
        allOfficeStaffs.add(new OfficeStaff(3, "Md.Sabbir Reza","Account Assistant (Contractual)" ,"+88"));




        //  Data for the Class 4 Staffs.............................
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Dulal Chandra Mali","Guard" ,"+8801712026493"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Samsul Islam","MLSS" ,"+8801787774662"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Nabi Ullah","Lab. Assistnant" ,"+8801917193347"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mizanur Rahaman","MLSS" ,"+8801718074088"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Muraduzzaman Akanda","MLSS" ,"+8801712059358"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Jalal Uddin Ahamed","MLSS" ,"+8801721063189"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Nurul Amin","MLSS" ,"+8801927267581"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Babul Akter","MLSS" ,"+8801716376341"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mokbul Ahamed","MLSS" ,"+8801913901372"));
        allOfficeStaffs.add(new OfficeStaff(4, "Asafunnassa","Labriary. Assistnant" ,"+8801199059504"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Sankor Chandro Mali","Lab. Assistnant" ,"+8801716553489"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abul Kashem","MLSS" ,"+8801864977610"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri jagganath Chandra Mali","Clenear" ,"+8801914716568"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Nurun Nabi","Lab. Assistnant" ,"+8801725832332"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Ismail Hossain","Clenear" ,"+8801709944538"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Shahinur Rahaman","MLSS" ,"+8801915863236"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Nepal Chandra Mali","Clenear" ,"+8801812387486"));
        allOfficeStaffs.add(new OfficeStaff(4, "Srimoti Kanon Rani","Clenear" ,"+8801937581940"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Tazul Islam","Boy" ,"+8801818293743"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abu Tahair","Moajjin" ,"+8801818376257"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Khairul Alam","MLSS" ,"+8801818177530"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdur Rahim","MLSS" ,"+8801819001692"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Sofi Ullah","Moajjin" ,"+8801912495297"));
        allOfficeStaffs.add(new OfficeStaff(4, "Firoza Begum","MLSS" ,"+8801710168197"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Joynal Abadin","Guardener" ,"+8801957449014"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Seddik Mia","MLSS" ,"+8801921869488"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Firoz Mia","MLSS" ,"+8801779790222"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Kabir Hossain - 1","MLSS" ,"+8801716897949"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Azgor Ali","Guard" ,"+801914130265"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Amin Ullah","Cook" ,"+8801817545285"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Badol Chandra Das","Healper(Bus)" ,"+8801736711002"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Kamal Hossain","MLSS" ,"+8801830875692"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abul Kashem - 4","MLSS" ,"+8801712393405"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mozammel Haque - 1","MLSS" ,"+8801718953780"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abu Sufian","MLSS" ,"+8801939693588"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mostafa","MLSS" ,"+8801813522318"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Sondip Kumar Das","Clenear" ,"+8801957060780"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Badol Chandra Das ","Clenear" ,"+8801814735204"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Harun","MLSS" ,"+8801728985191"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Tajul Islam","MLSS" ,"+8801914955427"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdur Rob","MLSS" ,"+8801712653792"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Shafikur Rahaman","MLSS" ,"+8801917849900"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Sultan Hossain","MLSS" ,"+8801687688705"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Golap Mostofa","Guard" ,"+8801719922761"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdul Motin","MLSS" ,"+8801921361554"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Azizul Haque","MLSS" ,"+8801721076367"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Sadak Ali","MLSS" ,"+8801949094209"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abu Said Mondol","MLSS" ,"+8801712607990"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Kobir Hossain","MLSS" ,"+8801819148873"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Jamal Hossain","MLSS" ,"+8801840606185"));
        allOfficeStaffs.add(new OfficeStaff(4, "Subal Leonat Gomaze","MLSS" ,"+8801719611627"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Shahinur Islam","MLSS" ,"+8801735239257"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mozzamel Haque","MLSS" ,"+8801717313923"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdul Wahab","MLSS" ,"+8801721868439"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Bodrul Alam","MLSS" ,"+8801747359882"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Harun-or-Rashid","MLSS" ,"+801758747211"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Jahingir Hossain Molla","Guard" ,"+8801925894123"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Mamunor Rashid","MLSS" ,"+8801983620258"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Shohag Rana","MLSS" ,"+8801784660707"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Ali Akbar","MLSS" ,"+8801724742393"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdullah-Al-Mamun","Electrician assistant" ,"+8801916329727"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Aynal Haque ","MLSS" ,"+8801927800722"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Rezaul Karim","MLSS" ,"+8801925493942"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Manik Hossain","MLSS" ,"+8801718728618"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Moznu Mia","MLSS" ,"+8801925731628"));
        allOfficeStaffs.add(new OfficeStaff(4, "Amin Mia","Guard" ,"+8801927125527"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Soharab Hossain","Guard" ,"+8801833073749"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Sujon Mia","MLSS" ,"+8801925551005"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Samol Chandra Mali","Guard" ,"+8801716368650"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Uttom Kumar Mali","Clenear" ,"+8801924832732"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Sujon Malakar","Clenear" ,"+8801811315616"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Surish Chandra Das","Clenear" ,"+8801710413972"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Humaian Kabir","MLSS" ,"+8801720959540"));
        allOfficeStaffs.add(new OfficeStaff(4, "Modina Begum","Aayya" ,"+8801746011418"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Golam Mostofa","Guard" ,"+8801934938372"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Firoz Alam","MLSS" ,"+8801738128788"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Salim Mia","MLSS" ,"+8801942312115"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Abdul Malik","MLSS" ,"+8801912717613"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Netai Chandra Das","Clenear" ,"+8801763684935"));
        allOfficeStaffs.add(new OfficeStaff(4, "Sri Sumon Das","Clenear" ,"+8801948488258"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Azaharul Islam","Guard" ,"+8801924447662"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Rubel Mia","MLSS" ,"+8801966353871"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Appel Mahamud","MLSS" ,"+8801945676657"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Rajib Hossain","Guard" ,"+8801622325286"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Tarikul Islam","Guard" ,"+8801782621436"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Rashadul Islam","MLSS" ,"+8801957302104"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Forhad Hossain","MLSS" ,"+8801989298497"));
        allOfficeStaffs.add(new OfficeStaff(4, "SamsurnNahar","Aayya" ,"+801728641477"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Salim Ahamed","MLSS" ,"+8801915100137"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Roman Sorkar","MLSS" ,"+8801828493249"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Belal Hossain","MLSS" ,"+8801862368752"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Somrat Babor","MLSS" ,"+8801986859058"));
        allOfficeStaffs.add(new OfficeStaff(4, "Md.Babul","Clenear" ,"+8801675939188"));
        allOfficeStaffs.add(new OfficeStaff(4, "Ranjit Chandra Das","Clenear" ,"+8801926645124"));
        allOfficeStaffs.add(new OfficeStaff(4, "Anika Akter","MLSS" ,"+8801729367204"));



    }
}
