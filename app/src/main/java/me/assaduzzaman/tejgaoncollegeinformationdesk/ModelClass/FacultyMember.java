package me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass;

public class FacultyMember {

   private int departmentId;
   private String departmentName;
   private String facultyMemberName;
   private String facultyMemberDesignation;
   private String facultyMemberPhone;

    public FacultyMember(int departmentId, String departmentName, String facultyMemberName, String facultyMemberDesignation, String facultyMemberPhone) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.facultyMemberName = facultyMemberName;
        this.facultyMemberDesignation = facultyMemberDesignation;
        this.facultyMemberPhone = facultyMemberPhone;
    }


    public int getDepartmentId() {
        return departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public String getFacultyMemberName() {
        return facultyMemberName;
    }

    public String getFacultyMemberDesignation() {
        return facultyMemberDesignation;
    }


    public String getFacultyMemberPhone() {
        return facultyMemberPhone;
    }





}
