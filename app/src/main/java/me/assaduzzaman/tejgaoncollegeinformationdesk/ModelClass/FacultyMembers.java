package me.assaduzzaman.tejgaoncollegeinformationdesk.ModelClass;

import java.util.ArrayList;

public class FacultyMembers {

    ArrayList<FacultyMember> allFacultyMembers;

    public ArrayList<FacultyMember> getFacultyMembersById(int id) {

        ArrayList<FacultyMember> facultyMembers=new ArrayList<>();

        for (int i=0; i< allFacultyMembers.size(); i++)
        {
            if(allFacultyMembers.get(i).getDepartmentId() == id)
                facultyMembers.add(allFacultyMembers.get(i));

        }

        return facultyMembers;
    }

    public FacultyMembers() {

        allFacultyMembers=new ArrayList<>();

        allFacultyMembers.add(new FacultyMember(1,"Accounting", "Md. Anowar-Us-Samad","Professor and Head of Department" ,"+8801727777429"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Md. Zaker Hossain Bhuiyan","Professor","+8801819137244"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Khandakar Rezaul Karim","Assistant Professor","+8801716734309"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","A.K.M Kharshed Alam","Assistant Professor" ,"+8801817543301"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Mr. Monzur Morshed Khan","Assistant Professor","+8801913982470"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Mr. Faruk Ahmed","Assistant Professor" ,"+8801718596916"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Md. Anowar Hossain","Lecturer","+8801717159589"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Yasmin Sultana","Lecturer" ,"+8801680547264"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Md. Abduzzaher","Lecturer" ,"+8801912131236"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Md. Enamul Haque","Lecturer","+8801922797037"));
        allFacultyMembers.add(new FacultyMember(1,"Accounting","Md. Hafizur Rahman","Assistant Professor" ,"+8801917166176"));

        allFacultyMembers.add(new FacultyMember(2,"Bangla","Md. Abdul Mokit","Associate Professor & Head of the Department" ,"+8801556305325"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","S.M. Ashraful Alam","Associate Professor ","+8801712184245"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Shanaz Akther","Associate Professor ","+8801916047178"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla", "Rahima Begum","Associate Professor","+8801834664301"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Shameem Akhter Khan","Lecturer","+8801817030242"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Olifa Yesmin","Lecturer" ,"+8801915875853"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Amimul Haque","Lecturer","+8801920294968"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Misbah Uddin","Lecturer","+8801918687908"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Kamal Chowdhury","Lecturer","+8801710665515"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Shamsi Ara Begum","Lecturer","+8801752066757"));
        allFacultyMembers.add(new FacultyMember(2,"Bangla","Adhir Chandro Sarker","Lecturer","+8801812528299"));

        allFacultyMembers.add(new FacultyMember(3,"BBA", "Md. Kamrul Hossain","Lecturer & Head of the Department","+8801879920955"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Syeda Samzila Atik","Lecturer","+8801732574140"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Md. Shah Alam","Lecturer","+8801687022528"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Ahsan Uddin","Lecturer","+8801710895575"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Md. Imran Khan","Lecturer","+8801843824143"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Muhammad Asem","Lecturer","+880"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Fatema Khatun","Lecturer","+8801302897958"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Nafisa Tanjia Rahman ","Lecturer","+8801841993665"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Md. Rajib","Lecturer","+8801766804809"));
        allFacultyMembers.add(new FacultyMember(3,"BBA", "Tuhin Hossain ","Lecturer","+8801854857564"));

        allFacultyMembers.add(new FacultyMember(4,"Botany", "Mir Sarifa Khanam","Assistant Professor & Head of the Department","+8801820535440"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Samsunnahar","Assistant Professor","+8801915646422"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Mohammad Harun or Rashid","Professor","+8801711566797"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Sufia Rahman","Lecturer","+8801816744022"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Kum Kum Ahmed","Lecturer","+8801912387125"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Abdullah Al-Mahmud","Lecturer","+8801520090741"));
        allFacultyMembers.add(new FacultyMember(4,"Botany", "Md.Abdul Haque","Lecturer","+8801911246640"));

        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Md. Morshed Alam","Professor & Head of the Department","+8801819435547"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Hasina Parvin","Assistant Professor","+8801715181009"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Mohammad Nuruzzaman  Masum","Lecturer","+8801855944295"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Mr. Sharif Niaz","Lecturer","+8801917041744"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Farjana Akhter Noor","Lecturer","+8801717593971"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Nazmul Karim Faruki","Lecturer","+8801796261641"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Zahirul Islam","Lecturer","+8801723704872"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Farzana Sultana","Lecturer","+8801714963602"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Kamrul Hasan","Lecturer","+8801684867565"));
        allFacultyMembers.add(new FacultyMember(5,"Bio-Chemistry", "Md. Mustafizur Rahman","Demonstrator","+8801715702525 "));

        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Md. Anawar Hossain","Professor & Head of the Department","+8801704031085"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Mohammed Soliman","Assistant Professor","+8801552321664"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Silvia Khair","Assistant Professor","+8801819201770"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Liton Chandara Roy","Lecturer","+8801714861261"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Mohammad Rokibul Islam","Lecturer","+8801715004642"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Md.Assaduzzaman","Lecturer","+8801717921514 "));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Sazib Mia","Lecturer","+8801827592828"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Gaffar Ali Mondol","Lecturer","+8801708529907"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Md. Nazrul Islam Mozumder","Lecturer","+8801676522189"));
        allFacultyMembers.add(new FacultyMember(6,"Chemistry", "Obaidur Rahman","Demonstrator","+8801552445438"));

        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Abdul Hadi","Associate Professor and Head of Department","+8801793545760"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Aminul Haque","Associate Professor","+8801552303047"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Shamim Hossen","Lecturer","+8801722920045"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Moklesur Rahman","Lecturer","+8801717406684"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Nargis Sultana","Lecturer","+8801918901726"));
        //allFacultyMembers.add(new FacultyMember(7,"CSE","Renu Ahmed","Lecturer","+8801914262153"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Khayruzzaman","Lecturer","+8801731206005"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Kabel Uddin","Lecturer","+8801925928476"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Farzana Rahman","Lecturer","+8801817032996"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Saila Rahman","Lecturer","+8801720026983"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Fazle Lohani","Lecturer","+8801627811172"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Saifuzzaman Khan","Lecturer","+8801717849618"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Shafiqul Islam","Lecturer","+8801758309941"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Mirza Tahera","Lecturer","+8801914278111"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Rajibul Islam","Lecturer","+8801552332211"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Saud-Al-Abedin","Lecturer","+8801681735690"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Ishan Arefin Hossain","Lecturer","+8801926255256"));
        allFacultyMembers.add(new FacultyMember(7,"CSE","Md. Assaduzzaman Noor","Lecturer","+8801682777666 "));

        allFacultyMembers.add(new FacultyMember(8,"English", "Md. Abdul Mannan","Professor & Head of the Department","+888123312"));
        allFacultyMembers.add(new FacultyMember(8,"English", " Isharat Ali","Professor","+8801711931239"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Md. Khursheed Alam","Lecturer","+8801916045553"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Sikta Das","Assistant Professor","+8801711262754"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Md. Arfan Ali Khan","Assistant Professor","+8801715958923"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Shamsul Alam","Assistant Professor","+8801718011256"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Nibedita Bhandary","Assistant Professor","+8801718303551"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Sharmin Akhter","Lecturer","+8801712752632"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Ummay Habiba","Lecturer","+880186772990"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Mohammad Abadat Hossain","Lecturer","+8801921464446"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Shoma Rani Paul","Lecturer","+8801756087408"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Sabina Bashar","Lecturer","+8801734389220"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Kriti Kinkini","Lecturer","+8801923051616"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Israt Jahan Akhi","Lecturer","+8801672618772"));
        allFacultyMembers.add(new FacultyMember(8,"English", "Shahnaj Parvin","Lecturer","+8801777805152"));

        allFacultyMembers.add(new FacultyMember(9,"Economics", "Nazma Begum","Professor & Head of the Department","+8801711100335"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", " Md. Moshiur Rahman","Associate Professor","+8801776054093"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Suffa-A-Rumin","Associate Professor","+8801703246708"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Mohammad Surzzaman","Associate Professor","+8801715040548"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Happy Floria Bower","Assistant Professor","+8801711567940"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Nasima Ahmed","Assistant Professor","+8801715087677"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Naznin Afsana","Lecturer","+8801795568297"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Shaila Sharmin","Lecturer","+88:01552373426"));
        allFacultyMembers.add(new FacultyMember(9,"Economics", "Bilkis Begum","Lecturer","+8801732709289"));

        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Rehana Sharmin","Associate Professor & Head of the Department","+880822003571"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", " Tassadduq Ahmed Khan","Associate Professor","+8801819503744"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Sabir Amin Reza","Associate Professor","+8801737538689"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Kazi Shahriar-Bin-Shams","Assistant Professor","+8801712857939"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Abu Jafor Md. Kamal Uddin","Assistant Professor","+8801711125615"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Mohammad Rafiqul Islam","Lecturer","+8801762373838"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Sifat Rahman","Lecturer","+8801737800324"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Ferdous Ara","Lecturer","+8801921209927"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "Sharia Alam","Lecturer","+8801922999221"));
        allFacultyMembers.add(new FacultyMember(10,"Finance & Banking", "S.M. Mahbubul Islam","Lecturer","+8801913164033"));

        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Kazi Abul Mahmud","Professor & Head of the Department","+8801817053507"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", " A.K.M. Harunur Rashid","Associate Professor","+8801708516495"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Md. Oliur Rahman","Associate Professor","+8801715025366"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Nurnabi Al Mahmud","Assistant Professor","+8801819451104"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Syed Ahmed Sarkar","Assistant Professor","+8801817090096"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Fatema Anis","Lecturer","+8801865444414"));
        allFacultyMembers.add(new FacultyMember(11,"Geography & Environment", "Mithila Rahman","Lecturer","+8801712236897"));

        allFacultyMembers.add(new FacultyMember(12,"History", "Amina Begum","Professor & Head of the Department","+880199121363"));
        allFacultyMembers.add(new FacultyMember(12,"History", "Rafida Begum","Assistant Professor","+8801816284872"));
        allFacultyMembers.add(new FacultyMember(12,"History", "Mohammad Rafiqul Alam","Assistant Professor","+880718965031"));
        allFacultyMembers.add(new FacultyMember(12,"History", "Nazma Parveen","Lecturer","+8801758699600"));

        allFacultyMembers.add(new FacultyMember(13,"Home Economics", "Shamima Yesmin","Associate Professor & Head of the Department","+8801711335458"));
        allFacultyMembers.add(new FacultyMember(13,"Home Economics", "Zinat Shahana","Assistant Professor","+8801741140702"));
        allFacultyMembers.add(new FacultyMember(13,"Home Economics", "Molina Rani Gosh","Assistant Professor","+8801819234678"));
        allFacultyMembers.add(new FacultyMember(13,"Home Economics", "Naznin Chowdhury","Lecturer","+8801913783478"));
        allFacultyMembers.add(new FacultyMember(13,"Home Economics", "Habiba sulltana","Demonostrator","+880"));

        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Md. Shamsul Alam","Professor & Head of the Department","+880155054576"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Suraiya Banu","Professor","+8801711406924"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Saila Sharmeen","Lecturer","+88001959125313"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Khanom Hasina Zinat","Lecturer","+8801819255769"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Shamima Akter Bhuyan","Lecturer","+8801734864422"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Mahbubur Rahman","Lecturer","+8801733969285"));
        allFacultyMembers.add(new FacultyMember(14,"Islamic History", "Afjal Sarif","Lecturer","+8801833505364"));

        allFacultyMembers.add(new FacultyMember(15,"Islami Studies ", "A.S.M. Enayet Ullah","Professor & Head of the Department","+8801799443753"));
        //allFacultyMembers.add(new FacultyMember(15,"Islami Studies ", "Dr. Md. Shafikul Islam","Professor","+8801552323448"));
        allFacultyMembers.add(new FacultyMember(15,"Islami Studies", "Dr. Md.Moududur Rahman Atiqui","Assistant Professor","+8801716162001"));
        allFacultyMembers.add(new FacultyMember(15,"Islami Studies", "Abdus Zaher Mahmud","Lecturer","+8801711459016"));
        allFacultyMembers.add(new FacultyMember(15,"Islami Studies", "A.Z. Amin Ullah","Lecturer","+8801916580254"));
        allFacultyMembers.add(new FacultyMember(15,"Islami Studies", "A.F.M Kamal uddin","Lecturer","+8801755166626"));

        allFacultyMembers.add(new FacultyMember(16,"Management", "Mostafa Harun-Or-Rashid","Associate Professor & Head of the Department","+8801711108660"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Md. Hanif","Professor","+88011717067898"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Anisur Rahaman","Associate Professor","+88017119563329"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "MD. Shahidul Islam","Associate Professor","+8801711009725"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Md. Shohail Rana","Assistant Professor","+8801832473782"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Md. Anowrul Hoque","Assistant Professor","+8801914331375"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Md. Joynal Abedin","Assistant Professor","+8801817124491"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Farhana Sultana","Assistant Professor","+8801711276244"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Jakia Jahan Chowdhury","Assistant Professor","+8801721518919"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Bibi jaheda","Assistant Professor","+8801711331565"));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Tamanna Hoque","Lecturer","+8801684737574 "));
        allFacultyMembers.add(new FacultyMember(16,"Management", "Mir Mohammad Iqbal Hossain","Lecturer","+8801716621567"));

        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Meer Mohammad Dilwar Husain","Professor & Head of the Department","+8801715290782"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Md. Karim","Professor","+8801552636220"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "S.M. Mahbubur Rahman","Professor","+8801819142424"));
        //allFacultyMembers.add(new FacultyMember(17,"Marketing", "Md. Nazibullah Khan","Associate Professor","+8801718238989"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Mahbuba Begum","Assistant Professor","+8801819471431"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Nasrin Akter","Assistant Professor","+8801716194442"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Mir Lutfor Rahman","Assistant Professor","+8801717474965"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Nusrat Jahan","Lecturer","+8801771599480"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Arman Miah","Lecturer","+8801817114689"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Momin Miah","Lecturer","+8801918312066"));
        allFacultyMembers.add(new FacultyMember(17,"Marketing", "Tasnia Nabil Urmi","Lecturer","+8801797009964"));

        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "A.K.M. Ataur Rahman","Professor & Head of the Department","+8801711951917"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Sahajat Marina","Professor","+8801797341560"));
        // allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Ikramul Haque","Assistant Professor","+8801711113933"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Nasima Akter","Assistant Professor","+8801678054336"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Mohammad Shahidul Islam","Assistant Professor","+8801911096575"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Md. Mamunur Rashid","Lecturer","+8801913719893"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Subas Chandra Pal","Lecturer","+8801716202677"));
        allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Nilima Sultana","Lecturer","+8801910522300 "));
        //allFacultyMembers.add(new FacultyMember(18,"Mathematics", "Mohi Uddin Sheikh","Demonostrator","+8801712066310"));

        allFacultyMembers.add(new FacultyMember(19,"Physics", "Md. Arshadul Islam Saikat","Assistant Professor & Head of the Department","+8801911769548"));
        //allFacultyMembers.add(new FacultyMember(19,"Physics", "Dr. A.K.M. Fazlul Hoque","Professor","+8801672416577"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Ramkrishna Roy Chowdhury","Assistant Professor","+8801711286246"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Md.Shohel Rana","Lecturer","+8801721666412"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Suborna Sumaddar","Lecturer","+8801718514255"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Md.Shorab Hossion","Lecturer","+8801710635270"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Md.Abu Sayed","Lecturer","+8801925576626"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Ishrat Ara Roksana","Lecturer","+8801743836307"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Mahbub Ahmad","Demonostrator","+8801831120093"));
        allFacultyMembers.add(new FacultyMember(19,"Physics", "Farjana Akhtaer","Demonostrator","+088"));

        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Hosne Ara Begum","Professor & Head of the Department","+8801719674782"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Anjuman Ara","Professor","+8801715030990"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Munira Parvin","Lecturer","+8801552349820"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Md.Ershad Ali Rahman","Lecturer","+8801722671226"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Jony Miah","Lecturer","+8801711870476"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Rubina Akter","Lecturer","+8801758697818"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Md. Mamunur Rashid","Lecturer","+8801925904698"));
        allFacultyMembers.add(new FacultyMember(20,"Psychology", "Sagir Hossion Bhuiyan","Lecturer","+8801830848435"));


        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Md. Bodrul Momin","Associate Professor & Head of the Department","+8801628013713"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "A.B.M Golam Faruque","Professor","+880"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Rifat Sharmin","Lecturer","+8801716989120"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Mushfeqa Alam","Lecturer","+8801717074795"));
        //allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Manjurul Karim","Lecturer","+8801732480321"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Ayesha Begum Roni","Lecturer","+8801933241708"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "MD. Manjural Karim","Lecturer","+880"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Md. Hafizur Rohman","Lecturer","+8801741307088"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Golam Rabbani","Lecturer","+8801716478156"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Md. Habibul Basar","Lecturer","+8801515613566"));
        allFacultyMembers.add(new FacultyMember(21,"Political Science ", "Sharmin AKter","Lecturer","+8801971737182"));

        allFacultyMembers.add(new FacultyMember(22,"Philosophy", "Dr.Razia Khanam","Lecturer & Head of the Department","+8801718058316"));
        allFacultyMembers.add(new FacultyMember(22,"Philosophy", "Afroza Sultana","Lecturer","+8801777658658"));
        allFacultyMembers.add(new FacultyMember(22,"Philosophy", "Monuar Hossain","Lecturer","+8801722453125"));
        allFacultyMembers.add(new FacultyMember(22,"Philosophy", "Sajib Kumar Bosu","Lecturer","+8801705531184"));

        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Dr. Anowarul Kabir","Professor & Head of the Department","+8801711192986"));
        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Md. Golam Mowla","Professor","+8801817506371"));
        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Md. Kamrul Alam","Assistant Professor","+8801817003212"));
        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Md. Iftekhar Uddin","Lecturer","+8801552479563"));
        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Md. Ataus Samad","Lecturer","+8801712730020"));
        allFacultyMembers.add(new FacultyMember(23,"Statistics", "Md. Shafiqul Islam","Lecturer","+8801552403256"));

        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Sayeda Maksuda Noor","Professor & Head of the Department","+8801731300315"));
        //allFacultyMembers.add(new FacultyMember(24,"Sociology", "Prof. Saki Nasrin","Associate Professor","+8801817 630689"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Md.Bazlur Rashid","Lecturer","+8801719475705"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Md. Meherul Hasan","Lecturer","+8801711 963418"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Jelly Roksasna","Lecturer","+8801749240078"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Rashida Jahan","Lecturer","+8801716164304"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Md.Rasheduzzaman","Lecturer","+8801711519303"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Md. Jahangir Alam","Lecturer","+8801814 374679"));
        allFacultyMembers.add(new FacultyMember(24,"Sociology", "Md. Abdus Satter","Lecturer","+8801719870313"));

        //allFacultyMembers.add(new FacultyMember(25,"Social Work", "Mr. Dr. A H M Mahbubur Rahman","Professor & Head of the Department","+8801711243136"));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Mahmuda Begum","Associate Professor",""));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Farida Akhter","Lecturer & Head of the Department","+8801739440100"));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Sharmin Nahar","Lecturer","+8801735997767"));
        //allFacultyMembers.add(new FacultyMember(25,"Social Work", "Sayed Asraf Ali","Lecturer","+8801720940208"));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Mr. Nargis Khatun","Lecturer","+8801926675307"));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Mokarroma khatum","Lecturer","+8801998606599"));
        allFacultyMembers.add(new FacultyMember(25,"Social Work", "Shah Md. Salim","Lecturer","+8801711033071"));

        allFacultyMembers.add(new FacultyMember(26,"TMS", "S.A.Faruk Ahmed","Professor & Head of the Department","+8801711707486"));
        allFacultyMembers.add(new FacultyMember(26,"TMS", "Dr. A.K.M.Zaharabi","Lecturer","+8801790127617"));
        allFacultyMembers.add(new FacultyMember(26,"TMS", "Nawrin Sazzad","Lecturer","+8801670707722"));
        //allFacultyMembers.add(new FacultyMember(26,"TMS", "Laila Ferdous","Lecturer","+8801723803562"));
        allFacultyMembers.add(new FacultyMember(26,"TMS", "Syed Md.Zubair Ahmed","Lecturer","+8801710489542"));
        allFacultyMembers.add(new FacultyMember(26,"TMS", "Md.Atikul Islam","Lecturer","+880167540075"));
        allFacultyMembers.add(new FacultyMember(26,"TMS", "Lota Sumaddaer","Lecturer","+8801728212151"));

        allFacultyMembers.add(new FacultyMember(27,"THM", "Md. Azizur Rahman","Lecturer & Head of the Department","+8801716208221"));
        allFacultyMembers.add(new FacultyMember(27,"THM", "Mohammad Javed Hossain","Lecturer","+880172779617"));
        allFacultyMembers.add(new FacultyMember(27,"THM", "Mahbuba Sultana","Lecturer","+8801670707722"));
        allFacultyMembers.add(new FacultyMember(27,"THM", "Md. Zaved Hossain","Lecturer","+8801727796517"));
        allFacultyMembers.add(new FacultyMember(27,"THM", "Adnam Noor","Lecturer","+880"));

        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Umme Salma Momotaz Ara,","Asso.Professor & Head of the Department","+8801717579034"));
        //allFacultyMembers.add(new FacultyMember(28,"Zoology", "Rehana Begum ","Assistant Professor","+880"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Tanzima Helaly","Assistant Professor","+8801920702801"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Afroz Fatema","Assistant Professor","+8801911593277"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Rokhsana Afroz","Assistant Professor","+8801814795501"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Mahbubur Rahman","Lecturer","+8801731797622"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "A.B.M. Faizul Mobin","Lecturer","+8801819291149"));
        allFacultyMembers.add(new FacultyMember(28,"Zoology", "Md. Amir Hossion","Lecturer","+8801712207143 "));

    }
}
