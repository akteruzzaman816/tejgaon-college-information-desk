package me.assaduzzaman.tejgaoncollegeinformationdesk;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.AboutActivity;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.AuthorityActivity;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.ContactActivity;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.CustomDrawerButton;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.FacultyActivity;
import me.assaduzzaman.tejgaoncollegeinformationdesk.Activity.StaffActivity;
public class MainActivity extends AppCompatActivity {
    CustomDrawerButton customDrawerButton;
    DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawer= (DrawerLayout)findViewById(R.id.drawer_layout);
        customDrawerButton = (CustomDrawerButton)findViewById(R.id.menuimage);
        customDrawerButton.setDrawerLayout( drawer );
        customDrawerButton.getDrawerLayout().addDrawerListener( customDrawerButton );
        customDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDrawerButton.changeState();
            }
        });





    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }






    public void authority(View view) {
         startActivity(new Intent(MainActivity.this,AuthorityActivity.class));
     }

     public void faculty(View view) {
         startActivity(new Intent(MainActivity.this,FacultyActivity.class));
     }

     public void staff(View view) {
         startActivity(new Intent(MainActivity.this,StaffActivity.class));
     }

     public void about(View view) {
         startActivity(new Intent(MainActivity.this,AboutActivity.class));
     }

     public void contact(View view) {
         startActivity(new Intent(MainActivity.this,ContactActivity.class));
     }

         }